package installer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class ServiceFundamentalFileExtractor {
    private String programInstallLocation;
    private String serviceName;
    private String installerName;

    public ServiceFundamentalFileExtractor(String programInstallLocation, String serviceName, String installerName){
        this.programInstallLocation = programInstallLocation;
        this.serviceName = serviceName;
        this.installerName = installerName;
    }

    public void createInstallDirectory(){
        File serviceInstallDirectory = new File(programInstallLocation + "/" + serviceName);
        if(!serviceInstallDirectory.exists()){
            serviceInstallDirectory.mkdir();
        }
    }

    public void copyFilesFromInstaller(){
        try{
            Files.copy(
                    ClassLoader.getSystemClassLoader().getResourceAsStream("prunsrv.exe"),
                    Paths.get(programInstallLocation + "/" + serviceName + "/prunsrv.exe"),
                    StandardCopyOption.REPLACE_EXISTING
            );
            Files.copy(
                    Paths.get(System.getProperty("user.dir") + "/" + this.installerName),
                    Paths.get(programInstallLocation + "/" + serviceName + "/" + serviceName+ ".jar"),
                    StandardCopyOption.REPLACE_EXISTING
            );
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}

package installer;

import java.util.List;

import static spark.Spark.get;

public class ServiceInstaller {

    private ServiceFundamentalFileExtractor serviceFundamentalFileExtractor;
    private BatFileServiceInstaller batFileServiceInstaller;

    public ServiceInstaller(String installerName, String programInstallLocation, String serviceName, List<String> startParams){
        serviceFundamentalFileExtractor = new ServiceFundamentalFileExtractor(programInstallLocation, serviceName, installerName);
        batFileServiceInstaller = new BatFileServiceInstaller(serviceName, programInstallLocation, startParams);
    }

    public void execute(){
        setupDirectoryAndExtractFiles();
        createAndRunBatFile();
    }

    private void setupDirectoryAndExtractFiles() {
        serviceFundamentalFileExtractor.createInstallDirectory();
        serviceFundamentalFileExtractor.copyFilesFromInstaller();
    }

    private void createAndRunBatFile() {
        batFileServiceInstaller.writeInstallerScript();
        batFileServiceInstaller.runBatFile();
    }

}

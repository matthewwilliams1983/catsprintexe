package installer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class BatFileServiceInstaller {

    private String echoOption;
    private String serviceNameBat;
    private String prunsrvLocation;
    private String jvmLocation;
    private String classPath;
    private String startUpMode;
    private String startMode;
    private String startClass;
    private String startMethod;
    private String stopMode;
    private String stopClass;
    private String stopMethod;
    private String startParams;
    private String jvmms;
    private String jvmmx;
    private String jvmss;
    private String jvmOption;
    private String installBatLine;
    private String executeServiceLine;
    private String exitOption;
    private File batFile;

    private String serviceName;
    private String programInstallLocation;

    public BatFileServiceInstaller(String serviceName, String programInstallLocation, List<String> startParams){

        this.batFile = new File(programInstallLocation + "/" + serviceName + "/installService.bat");
        this.echoOption = "@ECHO OFF\n";
        this.serviceNameBat =  "set SERVICE_NAME=" + serviceName + "\n";
        this.prunsrvLocation = "set PR_INSTALL=" + programInstallLocation + "/" + serviceName + "/prunsrv.exe\n";
        this.jvmLocation = "set PR_JVM=" + System.getProperty("java.home") + "/bin/server/jvm.dll\n";
        this.classPath = "set PR_CLASSPATH=" +  programInstallLocation + "/" + serviceName + "/" + serviceName + ".jar\n";
        this.startUpMode = "set PR_STARTUP=auto\n";
        this.startMode = "set PR_STARTMODE=jvm\n";
        this.startClass = "set PR_STARTCLASS=service.Service\n";
        this.startMethod = "set PR_STARTMETHOD=start\n";
        this.stopMode = "set PR_STOPMODE=jvm\n";
        this.stopClass = "set PR_STOPCLASS=service.Service\n";
        this.stopMethod = "set PR_STOPMETHOD=stop\n";

        this.startParams = "set PR_STARTPARAMS=";
        for(String param: startParams){
            this.startParams = this.startParams + param + '#';
        }
        this.startParams += "\n";

        this.jvmms = "set PR_JVMMS=256\n";
        this.jvmmx = "set PR_JVMMX=1024\n";
        this.jvmss = "set PR_JVMSS=4000\n";
        this.jvmOption = "set PR_JVMOPTIONS=-Duser.language=DE;-Duser.region=de\n";
        this.installBatLine = programInstallLocation + "/" + serviceName + "/prunsrv.exe //IS//%SERVICE_NAME%\n";
        this.executeServiceLine =  programInstallLocation + "/" + serviceName + "/prunsrv.exe //ES//%SERVICE_NAME%\n";
        this.exitOption = "EXIT";

        this.serviceName = serviceName;
        this.programInstallLocation = programInstallLocation;
    }

    public void writeInstallerScript(){
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter(getBatFile()));
            printWriter.write(this.toString());
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void runBatFile(){
        try {
            String[] commandInstall = {
                    "cmd.exe", "/C", "Start", "/B",
                    this.programInstallLocation + "/" + this.serviceName + "/installService.bat",
                    "install"
            };
            Runtime.getRuntime().exec(commandInstall);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getEchoOption() {
        return echoOption;
    }

    public void setEchoOption(String echoOption) {
        this.echoOption = echoOption;
    }

    public String getServiceNameBat() {
        return serviceNameBat;
    }

    public void setServiceNameBat(String serviceNameBat) {
        this.serviceNameBat = serviceNameBat;
    }

    public String getPrunsrvLocation() {
        return prunsrvLocation;
    }

    public void setPrunsrvLocation(String prunsrvLocation) {
        this.prunsrvLocation = prunsrvLocation;
    }

    public String getJvmLocation() {
        return jvmLocation;
    }

    public void setJvmLocation(String jvmLocation) {
        this.jvmLocation = jvmLocation;
    }

    public String getClassPath() {
        return classPath;
    }

    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }

    public String getStartUpMode() {
        return startUpMode;
    }

    public void setStartUpMode(String startUpMode) {
        this.startUpMode = startUpMode;
    }

    public String getStartMode() {
        return startMode;
    }

    public void setStartMode(String startMode) {
        this.startMode = startMode;
    }

    public String getStartClass() {
        return startClass;
    }

    public void setStartClass(String startClass) {
        this.startClass = startClass;
    }

    public String getStartMethod() {
        return startMethod;
    }

    public void setStartMethod(String startMethod) {
        this.startMethod = startMethod;
    }

    public String getStopMode() {
        return stopMode;
    }

    public void setStopMode(String stopMode) {
        this.stopMode = stopMode;
    }

    public String getStopMethod() {
        return stopMethod;
    }

    public void setStopMethod(String stopMethod) {
        this.stopMethod = stopMethod;
    }

    public String getStartParams() {
        return startParams;
    }

    public void setStartParams(String startParams) {
        this.startParams = startParams;
    }

    public String getJvmms() {
        return jvmms;
    }

    public void setJvmms(String jvmms) {
        this.jvmms = jvmms;
    }

    public String getJvmmx() {
        return jvmmx;
    }

    public void setJvmmx(String jvmmx) {
        this.jvmmx = jvmmx;
    }

    public String getJvmss() {
        return jvmss;
    }

    public void setJvmss(String jvmss) {
        this.jvmss = jvmss;
    }

    public String getJvmOption() {
        return jvmOption;
    }

    public void setJvmOption(String jvmOption) {
        this.jvmOption = jvmOption;
    }

    public String getInstallBatLine() {
        return installBatLine;
    }

    public void setInstallBatLine(String installBatLine) {
        this.installBatLine = installBatLine;
    }

    public String getExecuteServiceLine() {
        return executeServiceLine;
    }

    public void setExecuteServiceLine(String executeServiceLine) {
        this.executeServiceLine = executeServiceLine;
    }

    public String getStopClass() {
        return stopClass;
    }

    public void setStopClass(String stopClass) {
        this.stopClass = stopClass;
    }

    public String getExitOption() {
        return exitOption;
    }

    public void setExitOption(String exitOption) {
        this.exitOption = exitOption;
    }

    public File getBatFile() {
        return batFile;
    }

    public void setBatFile(File batFile) {
        this.batFile = batFile;
    }

    public String toString(){

        return this.echoOption + this.serviceNameBat + this.prunsrvLocation + this.jvmLocation + this.classPath +
                this.startUpMode + this.startMode + this.startClass + this.startMethod + this.stopMode +
                this.stopClass + this.stopMethod + this.startParams + this.jvmms + this.jvmmx + this.jvmss +
                this.jvmOption + this.installBatLine + this.executeServiceLine + this.exitOption;
    }

}

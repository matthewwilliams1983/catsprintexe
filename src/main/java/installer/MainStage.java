package installer;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class MainStage extends Application {

    private TextField ipAddressForPrinterField;
    private TextField installLocationField;

    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {



        BorderPane borderPane = new BorderPane();
         /*
            boarderPane
            ---------Top---------
            l                   r
            e       center      i
            f                   g
            t                   h
            --------Bottom-------
        */

        Scene scene = new Scene(borderPane, 500, 500);

        HBox hBoxTop = setTopOfBorderPane();
        GridPane gridPane = setCenterOfBorderPane();
        HBox hBoxBottom = setBottomOfBorderPane();

        borderPane.setTop(hBoxTop);
        borderPane.setCenter(gridPane);
        borderPane.setBottom(hBoxBottom);

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    private HBox setTopOfBorderPane() {
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER);
        hbox.setPadding(new Insets(10, 10, 50, 10));
        final Label title = new Label("CATS Print Service Installer");
        title.setFont(new Font("Arial", 30));
        hbox.getChildren().add(title);
        return hbox;
    }

    private GridPane setCenterOfBorderPane() {
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 0, 0, 15));
        gridPane.setHgap(5);
        gridPane.setVgap(20);

        final Label ipAddressLabel = new Label("IP ADDRESS");

        ipAddressForPrinterField = new TextField();
        ipAddressForPrinterField.setPromptText("1.1.1.1");
        ipAddressForPrinterField.setPrefWidth(200);
        ipAddressForPrinterField.setFocusTraversable(false);

        final Label installLocationLabel = new Label("Install Location");

        installLocationField = new TextField();
        installLocationField.setPromptText("C:/Hvat");
        installLocationField.setPrefWidth(200);
        installLocationField.setFocusTraversable(false);


        gridPane.add(ipAddressLabel, 0, 1);
        gridPane.add(ipAddressForPrinterField, 1, 1);
        gridPane.add(installLocationLabel, 0, 2);
        gridPane.add(installLocationField, 1, 2);

        return gridPane;
    }

    private HBox setBottomOfBorderPane() {
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(25));
        Button buttonInstallService = new Button("Install service");
        buttonInstallService.setOnAction(e -> {
            installService();
        });
        hBox.setAlignment(Pos.BOTTOM_RIGHT);
        hBox.getChildren().add(buttonInstallService);
        return hBox;
    }//EoM setBottomOfBorderPane----------------------------------------------------------------------------------------

    private void installService() {

        List<String> startParams = new ArrayList<>();
        setStartParams(startParams);

        //BRANDON CHANGE THESE
        PrintServiceInstaller printServiceInstaller = new PrintServiceInstaller(
                "CatsPrintServiceInstaller.exe",//This name needs to be the same name in the pom file for <outfile></outfile>
                "C:/Hvat",//Where you want to have the service reside
                "CatsPrintService",//The name of the service
                 startParams);//Params that should be passed to the service

        printServiceInstaller.execute();
    }//EoM installService-----------------------------------------------------------------------------------------------

    private void setStartParams(List<String> startParams) {
        startParams.add(ipAddressForPrinterField.getText());
        startParams.add(installLocationField.getText());
    }
}//EoC installer.MainStage--------------------------------------------------------------------------------------------------------

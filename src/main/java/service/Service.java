package service;

public class Service {

    private static boolean stop = false;

    public static void start(String[] args){
        (new Thread(new RestService())).start();
    }


    public static void stop(String[] args) {
        System.out.println("stop");
        stop = true;
    }

    public static void main(String[] args) {
        if ("start".equals(args[0])) {
            start(args);
        } else if ("stop".equals(args[0])) {
            stop(args);
        }
    }

}

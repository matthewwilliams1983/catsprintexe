package service;

import static spark.Spark.get;

public class RestService implements Runnable{
    @Override
    public void run() {
        get("/hello", (req, res)->"Hello, world");
    }
}
